package example.com.haptikproducthunt.network.callbacks;

import example.com.haptikproducthunt.network.BaseCallback;
import example.com.haptikproducthunt.network.ResultNotifiers;
import example.com.haptikproducthunt.network.apimodels.CommentResponse;
import example.com.haptikproducthunt.network.apimodels.PostResponse;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by Prakhar on 01-Jul-17.
 */

public class CommentCallback extends BaseCallback<CommentResponse> {

    public CommentCallback(ResultNotifiers.IBaseNotifier notifier) {
        super(notifier);
    }

    /**
     * login success response callback.
     *
     * @param model    response model
     * @param call     call
     * @param response response
     */

    @Override
    public void onSuccessResponse(CommentResponse model, Call<CommentResponse> call, Response<CommentResponse> response) {
        if (model != null) {
            notifyResponse(model);
        } else {
            notifyError("Failed to fetch response");
        }
    }


    /**
     * notify success response.
     *
     * @param model
     */
    private void notifyResponse(CommentResponse model) {
        if (mNotifier != null && mNotifier instanceof ResultNotifiers.ICommentResultNotifier) {
            ((ResultNotifiers.ICommentResultNotifier) mNotifier).onCommentReceiveResponse(model);
        }
    }
}

