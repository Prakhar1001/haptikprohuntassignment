

package example.com.haptikproducthunt.network.apimodels;

/**
 * Class used for common error response for all API calls
 */
public class APIError {

    private int statusCode;
    private String message;

    public APIError() {
    }

    public int status() {
        return statusCode;
    }

    public String message() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}