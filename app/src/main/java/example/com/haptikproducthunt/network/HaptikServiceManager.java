package example.com.haptikproducthunt.network;

import java.util.Map;

import example.com.haptikproducthunt.network.apimodels.CommentResponse;
import example.com.haptikproducthunt.network.apimodels.PostResponse;
import example.com.haptikproducthunt.network.callbacks.CommentCallback;
import example.com.haptikproducthunt.network.callbacks.PostCallback;
import retrofit2.Call;

/**
 * SingleTon class used to create request service for specific API call.
 */
public class HaptikServiceManager {
    private static HaptikServiceManager sManager;

    private HaptikServiceManager() {

    }


    public static HaptikServiceManager getInstance() {
        if (sManager == null) {
            sManager = new HaptikServiceManager();
        }
        return sManager;
    }

    /**
     * @param notifier PostResponse Result Notifier
     * @return Call
     */
    public Call performgetPostsForTodayOperation(ResultNotifiers.IPostResultNotifier notifier) {
        Call<PostResponse.Response> PostCall = HaptikServiceGenerator.getInstance().getService()
                .callPostForTodayAPI();
        PostCall.enqueue(new PostCallback(notifier));
        return PostCall;
    }


    /**
     * @param notifier PostResponse Result Notifier
     * @return Call
     */
    public Call performgetPostsForDaysAgoOperation(String daysAgo, ResultNotifiers.IPostResultNotifier notifier) {
        Call<PostResponse.Response> PostCall = HaptikServiceGenerator.getInstance().getService()
                .callPostForDaysAgoAPI(daysAgo);
        PostCall.enqueue(new PostCallback(notifier));
        return PostCall;
    }


    /**
     * @param notifier PostResponse Result Notifier
     * @return Call
     */
    public Call performgetPostsForSpecificDayOperation(String specificDate, ResultNotifiers.IPostResultNotifier notifier) {
        Call<PostResponse.Response> PostCall = HaptikServiceGenerator.getInstance().getService()
                .callPostForSpecificDayAPI(specificDate);
        PostCall.enqueue(new PostCallback(notifier));
        return PostCall;
    }

    /**
     * @param notifier PostResponse Result Notifier
     * @return Call
     */
    public Call performgetCommentsForPostOperation(Map<String, String> parameters, ResultNotifiers.ICommentResultNotifier notifier) {
        Call<CommentResponse> commentCall = HaptikServiceGenerator.getInstance().getService()
                .callCommentsForSpecificPostAPI(parameters);
        commentCall.enqueue(new CommentCallback(notifier));
        return commentCall;
    }


}