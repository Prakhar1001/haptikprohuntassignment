package example.com.haptikproducthunt.network.apimodels;

/**
 * Created by Prakhar on 01-Jul-17.
 */

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CommentResponse {

    @SerializedName("comments")
    @Expose
    private List<Comment> comments = null;

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }


    public class Comment {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("body")
        @Expose
        private String body;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("parent_comment_id")
        @Expose
        private Integer parentCommentId;
        @SerializedName("user_id")
        @Expose
        private Integer userId;
        @SerializedName("subject_id")
        @Expose
        private Integer subjectId;
        @SerializedName("child_comments_count")
        @Expose
        private Integer childCommentsCount;
        @SerializedName("url")
        @Expose
        private String url;
        @SerializedName("post_id")
        @Expose
        private Integer postId;
        @SerializedName("subject_type")
        @Expose
        private String subjectType;
        @SerializedName("sticky")
        @Expose
        private Boolean sticky;
        @SerializedName("votes")
        @Expose
        private Integer votes;
        @SerializedName("post")
        @Expose
        private Post post;
        @SerializedName("user")
        @Expose
        private User user;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getBody() {
            return body;
        }

        public void setBody(String body) {
            this.body = body;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public Integer getParentCommentId() {
            return parentCommentId;
        }

        public void setParentCommentId(Integer parentCommentId) {
            this.parentCommentId = parentCommentId;
        }

        public Integer getUserId() {
            return userId;
        }

        public void setUserId(Integer userId) {
            this.userId = userId;
        }

        public Integer getSubjectId() {
            return subjectId;
        }

        public void setSubjectId(Integer subjectId) {
            this.subjectId = subjectId;
        }

        public Integer getChildCommentsCount() {
            return childCommentsCount;
        }

        public void setChildCommentsCount(Integer childCommentsCount) {
            this.childCommentsCount = childCommentsCount;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public Integer getPostId() {
            return postId;
        }

        public void setPostId(Integer postId) {
            this.postId = postId;
        }

        public String getSubjectType() {
            return subjectType;
        }

        public void setSubjectType(String subjectType) {
            this.subjectType = subjectType;
        }

        public Boolean getSticky() {
            return sticky;
        }

        public void setSticky(Boolean sticky) {
            this.sticky = sticky;
        }

        public Integer getVotes() {
            return votes;
        }

        public void setVotes(Integer votes) {
            this.votes = votes;
        }

        public Post getPost() {
            return post;
        }

        public void setPost(Post post) {
            this.post = post;
        }

        public User getUser() {
            return user;
        }

        public void setUser(User user) {
            this.user = user;
        }

    }

    public class ImageUrl {

        @SerializedName("30px")
        @Expose
        private String _30px;
        @SerializedName("32px")
        @Expose
        private String _32px;
        @SerializedName("40px")
        @Expose
        private String _40px;
        @SerializedName("44px")
        @Expose
        private String _44px;
        @SerializedName("48px")
        @Expose
        private String _48px;
        @SerializedName("50px")
        @Expose
        private String _50px;
        @SerializedName("60px")
        @Expose
        private String _60px;
        @SerializedName("64px")
        @Expose
        private String _64px;
        @SerializedName("73px")
        @Expose
        private String _73px;
        @SerializedName("80px")
        @Expose
        private String _80px;
        @SerializedName("88px")
        @Expose
        private String _88px;
        @SerializedName("96px")
        @Expose
        private String _96px;
        @SerializedName("100px")
        @Expose
        private String _100px;
        @SerializedName("110px")
        @Expose
        private String _110px;
        @SerializedName("120px")
        @Expose
        private String _120px;
        @SerializedName("132px")
        @Expose
        private String _132px;
        @SerializedName("146px")
        @Expose
        private String _146px;
        @SerializedName("160px")
        @Expose
        private String _160px;
        @SerializedName("176px")
        @Expose
        private String _176px;
        @SerializedName("220px")
        @Expose
        private String _220px;
        @SerializedName("264px")
        @Expose
        private String _264px;
        @SerializedName("32px@2X")
        @Expose
        private String _32px2X;
        @SerializedName("40px@2X")
        @Expose
        private String _40px2X;
        @SerializedName("44px@2X")
        @Expose
        private String _44px2X;
        @SerializedName("88px@2X")
        @Expose
        private String _88px2X;
        @SerializedName("32px@3X")
        @Expose
        private String _32px3X;
        @SerializedName("40px@3X")
        @Expose
        private String _40px3X;
        @SerializedName("44px@3X")
        @Expose
        private String _44px3X;
        @SerializedName("88px@3X")
        @Expose
        private String _88px3X;
        @SerializedName("original")
        @Expose
        private String original;

        public String get30px() {
            return _30px;
        }

        public void set30px(String _30px) {
            this._30px = _30px;
        }

        public String get32px() {
            return _32px;
        }

        public void set32px(String _32px) {
            this._32px = _32px;
        }

        public String get40px() {
            return _40px;
        }

        public void set40px(String _40px) {
            this._40px = _40px;
        }

        public String get44px() {
            return _44px;
        }

        public void set44px(String _44px) {
            this._44px = _44px;
        }

        public String get48px() {
            return _48px;
        }

        public void set48px(String _48px) {
            this._48px = _48px;
        }

        public String get50px() {
            return _50px;
        }

        public void set50px(String _50px) {
            this._50px = _50px;
        }

        public String get60px() {
            return _60px;
        }

        public void set60px(String _60px) {
            this._60px = _60px;
        }

        public String get64px() {
            return _64px;
        }

        public void set64px(String _64px) {
            this._64px = _64px;
        }

        public String get73px() {
            return _73px;
        }

        public void set73px(String _73px) {
            this._73px = _73px;
        }

        public String get80px() {
            return _80px;
        }

        public void set80px(String _80px) {
            this._80px = _80px;
        }

        public String get88px() {
            return _88px;
        }

        public void set88px(String _88px) {
            this._88px = _88px;
        }

        public String get96px() {
            return _96px;
        }

        public void set96px(String _96px) {
            this._96px = _96px;
        }

        public String get100px() {
            return _100px;
        }

        public void set100px(String _100px) {
            this._100px = _100px;
        }

        public String get110px() {
            return _110px;
        }

        public void set110px(String _110px) {
            this._110px = _110px;
        }

        public String get120px() {
            return _120px;
        }

        public void set120px(String _120px) {
            this._120px = _120px;
        }

        public String get132px() {
            return _132px;
        }

        public void set132px(String _132px) {
            this._132px = _132px;
        }

        public String get146px() {
            return _146px;
        }

        public void set146px(String _146px) {
            this._146px = _146px;
        }

        public String get160px() {
            return _160px;
        }

        public void set160px(String _160px) {
            this._160px = _160px;
        }

        public String get176px() {
            return _176px;
        }

        public void set176px(String _176px) {
            this._176px = _176px;
        }

        public String get220px() {
            return _220px;
        }

        public void set220px(String _220px) {
            this._220px = _220px;
        }

        public String get264px() {
            return _264px;
        }

        public void set264px(String _264px) {
            this._264px = _264px;
        }

        public String get32px2X() {
            return _32px2X;
        }

        public void set32px2X(String _32px2X) {
            this._32px2X = _32px2X;
        }

        public String get40px2X() {
            return _40px2X;
        }

        public void set40px2X(String _40px2X) {
            this._40px2X = _40px2X;
        }

        public String get44px2X() {
            return _44px2X;
        }

        public void set44px2X(String _44px2X) {
            this._44px2X = _44px2X;
        }

        public String get88px2X() {
            return _88px2X;
        }

        public void set88px2X(String _88px2X) {
            this._88px2X = _88px2X;
        }

        public String get32px3X() {
            return _32px3X;
        }

        public void set32px3X(String _32px3X) {
            this._32px3X = _32px3X;
        }

        public String get40px3X() {
            return _40px3X;
        }

        public void set40px3X(String _40px3X) {
            this._40px3X = _40px3X;
        }

        public String get44px3X() {
            return _44px3X;
        }

        public void set44px3X(String _44px3X) {
            this._44px3X = _44px3X;
        }

        public String get88px3X() {
            return _88px3X;
        }

        public void set88px3X(String _88px3X) {
            this._88px3X = _88px3X;
        }

        public String getOriginal() {
            return original;
        }

        public void setOriginal(String original) {
            this.original = original;
        }

    }

    public class Post {


    }

    public class User {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("username")
        @Expose
        private String username;
        @SerializedName("headline")
        @Expose
        private String headline;
        @SerializedName("twitter_username")
        @Expose
        private Object twitterUsername;
        @SerializedName("website_url")
        @Expose
        private Object websiteUrl;
        @SerializedName("profile_url")
        @Expose
        private String profileUrl;
        @SerializedName("image_url")
        @Expose
        private ImageUrl imageUrl;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getHeadline() {
            return headline;
        }

        public void setHeadline(String headline) {
            this.headline = headline;
        }

        public Object getTwitterUsername() {
            return twitterUsername;
        }

        public void setTwitterUsername(Object twitterUsername) {
            this.twitterUsername = twitterUsername;
        }

        public Object getWebsiteUrl() {
            return websiteUrl;
        }

        public void setWebsiteUrl(Object websiteUrl) {
            this.websiteUrl = websiteUrl;
        }

        public String getProfileUrl() {
            return profileUrl;
        }

        public void setProfileUrl(String profileUrl) {
            this.profileUrl = profileUrl;
        }

        public ImageUrl getImageUrl() {
            return imageUrl;
        }

        public void setImageUrl(ImageUrl imageUrl) {
            this.imageUrl = imageUrl;
        }

    }
}