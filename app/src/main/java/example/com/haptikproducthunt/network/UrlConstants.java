package example.com.haptikproducthunt.network;

import example.com.haptikproducthunt.BuildConfig;

public class UrlConstants {

    public static boolean DEBUG_MODE = BuildConfig.DEBUG;


    public static final String POSTS_URL = "posts";
    public static final String COMMENTS_URL = "comments";


}
