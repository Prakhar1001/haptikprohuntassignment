package example.com.haptikproducthunt.network;


import java.io.IOException;
import java.util.concurrent.TimeUnit;

import example.com.haptikproducthunt.utils.Constants;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Singleton class provides service setup for network call using retrofit.
 */
class HaptikServiceGenerator {
    private Retrofit retrofit;
    private static HaptikServiceGenerator sServerInstance;

    private HaptikServiceGenerator() {
        OkHttpClient httpClient = new OkHttpClient.Builder()
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        Request.Builder ongoing = chain.request().newBuilder();
                        ongoing.addHeader("Content-Type", "application/json");
                        ongoing.addHeader("Authorization", Constants.getToken());
                        ongoing.addHeader("Accept", "application/json;versions=1");
                        ongoing.addHeader("Host", "api.producthunt.com");
                        return chain.proceed(ongoing.build());
                    }
                })
                .connectTimeout(Configuration.CONNECTION_TIME_OUT_MILI, TimeUnit.MILLISECONDS)
                .build();


        Retrofit.Builder builder =
                new Retrofit.Builder()
                        .baseUrl(Configuration.CONFIGURABLE_BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create());

        retrofit = builder
                .client(httpClient)
                .build();
    }

    /* Package Access */
    static HaptikServiceGenerator getInstance() {
        sServerInstance = new HaptikServiceGenerator();
        return sServerInstance;
    }

    /* Package Access */
    HaptikService getService() {
        return retrofit.create(HaptikService.class);
    }

    /* Package Access */
    Retrofit getRetrofit() {
        return retrofit;
    }


}