

package example.com.haptikproducthunt.network;



import java.util.Map;

import example.com.haptikproducthunt.network.apimodels.CommentResponse;
import example.com.haptikproducthunt.network.apimodels.PostResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

/**
 * Define all APIs here one by one. Every URL and API methods must be defined here.
 *
 */
interface HaptikService {


    @GET(UrlConstants.POSTS_URL)
    Call<PostResponse.Response> callPostForTodayAPI();

    @GET(UrlConstants.POSTS_URL)
    Call<PostResponse.Response> callPostForDaysAgoAPI(@Query("days_ago") String days_Ago);

    @GET(UrlConstants.POSTS_URL)
    Call<PostResponse.Response> callPostForSpecificDayAPI(@Query("day") String day);

    @GET(UrlConstants.COMMENTS_URL)
    Call<CommentResponse> callCommentsForSpecificPostAPI(@QueryMap Map<String, String> parameters);



}
