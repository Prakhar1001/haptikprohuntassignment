package example.com.haptikproducthunt.network;

/**
 * Created by Prakhar on 30-Jun-17.
 */

public class Configuration {
    public static final String CONFIGURABLE_BASE_URL = "https://api.producthunt.com/v1/";
    public static long CONNECTION_TIME_OUT_MILI = 50000;
}
