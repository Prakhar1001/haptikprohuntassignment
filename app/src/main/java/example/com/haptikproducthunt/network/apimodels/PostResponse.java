package example.com.haptikproducthunt.network.apimodels;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * POJO class handles both Request and Response for Login API
 */

public class PostResponse {

    public static class Response implements Parcelable{


        @SerializedName("posts")
        @Expose
        private List<Post> posts = null;

        protected Response(Parcel in) {
        }

        public static final Creator<Response> CREATOR = new Creator<Response>() {
            @Override
            public Response createFromParcel(Parcel in) {
                return new Response(in);
            }

            @Override
            public Response[] newArray(int size) {
                return new Response[size];
            }
        };

        public List<Post> getPosts() {
            return posts;
        }

        public void setPosts(List<Post> posts) {
            this.posts = posts;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
        }
    }

    public class ScreenshotUrl {

        @SerializedName("300px")
        @Expose
        private String _300px;
        @SerializedName("850px")
        @Expose
        private String _850px;

        public String get300px() {
            return _300px;
        }

        public void set300px(String _300px) {
            this._300px = _300px;
        }

        public String get850px() {
            return _850px;
        }

        public void set850px(String _850px) {
            this._850px = _850px;
        }

    }

    public class Thumbnail {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("media_type")
        @Expose
        private String mediaType;
        @SerializedName("image_url")
        @Expose
        private String imageUrl;
        @SerializedName("metadata")
        @Expose
        private Metadata metadata;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getMediaType() {
            return mediaType;
        }

        public void setMediaType(String mediaType) {
            this.mediaType = mediaType;
        }

        public String getImageUrl() {
            return imageUrl;
        }

        public void setImageUrl(String imageUrl) {
            this.imageUrl = imageUrl;
        }

        public Metadata getMetadata() {
            return metadata;
        }

        public void setMetadata(Metadata metadata) {
            this.metadata = metadata;
        }

    }


    public class Topic {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("slug")
        @Expose
        private String slug;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getSlug() {
            return slug;
        }

        public void setSlug(String slug) {
            this.slug = slug;
        }

    }

    public class User {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("username")
        @Expose
        private String username;
        @SerializedName("headline")
        @Expose
        private String headline;
        @SerializedName("twitter_username")
        @Expose
        private Object twitterUsername;
        @SerializedName("website_url")
        @Expose
        private Object websiteUrl;
        @SerializedName("profile_url")
        @Expose
        private String profileUrl;
        @SerializedName("image_url")
        @Expose
        private ImageUrl_ imageUrl;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getHeadline() {
            return headline;
        }

        public void setHeadline(String headline) {
            this.headline = headline;
        }

        public Object getTwitterUsername() {
            return twitterUsername;
        }

        public void setTwitterUsername(Object twitterUsername) {
            this.twitterUsername = twitterUsername;
        }

        public Object getWebsiteUrl() {
            return websiteUrl;
        }

        public void setWebsiteUrl(Object websiteUrl) {
            this.websiteUrl = websiteUrl;
        }

        public String getProfileUrl() {
            return profileUrl;
        }

        public void setProfileUrl(String profileUrl) {
            this.profileUrl = profileUrl;
        }

        public ImageUrl_ getImageUrl() {
            return imageUrl;
        }

        public void setImageUrl(ImageUrl_ imageUrl) {
            this.imageUrl = imageUrl;
        }

    }

    public class CurrentUser {

        @SerializedName("voted_for_post")
        @Expose
        private Boolean votedForPost;
        @SerializedName("commented_on_post")
        @Expose
        private Boolean commentedOnPost;

        public Boolean getVotedForPost() {
            return votedForPost;
        }

        public void setVotedForPost(Boolean votedForPost) {
            this.votedForPost = votedForPost;
        }

        public Boolean getCommentedOnPost() {
            return commentedOnPost;
        }

        public void setCommentedOnPost(Boolean commentedOnPost) {
            this.commentedOnPost = commentedOnPost;
        }

    }

    public class ImageUrl {

        @SerializedName("30px")
        @Expose
        private String _30px;
        @SerializedName("32px")
        @Expose
        private String _32px;
        @SerializedName("40px")
        @Expose
        private String _40px;
        @SerializedName("44px")
        @Expose
        private String _44px;
        @SerializedName("48px")
        @Expose
        private String _48px;
        @SerializedName("50px")
        @Expose
        private String _50px;
        @SerializedName("60px")
        @Expose
        private String _60px;
        @SerializedName("64px")
        @Expose
        private String _64px;
        @SerializedName("73px")
        @Expose
        private String _73px;
        @SerializedName("80px")
        @Expose
        private String _80px;
        @SerializedName("88px")
        @Expose
        private String _88px;
        @SerializedName("96px")
        @Expose
        private String _96px;
        @SerializedName("100px")
        @Expose
        private String _100px;
        @SerializedName("110px")
        @Expose
        private String _110px;
        @SerializedName("120px")
        @Expose
        private String _120px;
        @SerializedName("132px")
        @Expose
        private String _132px;
        @SerializedName("146px")
        @Expose
        private String _146px;
        @SerializedName("160px")
        @Expose
        private String _160px;
        @SerializedName("176px")
        @Expose
        private String _176px;
        @SerializedName("220px")
        @Expose
        private String _220px;
        @SerializedName("264px")
        @Expose
        private String _264px;
        @SerializedName("32px@2X")
        @Expose
        private String _32px2X;
        @SerializedName("40px@2X")
        @Expose
        private String _40px2X;
        @SerializedName("44px@2X")
        @Expose
        private String _44px2X;
        @SerializedName("88px@2X")
        @Expose
        private String _88px2X;
        @SerializedName("32px@3X")
        @Expose
        private String _32px3X;
        @SerializedName("40px@3X")
        @Expose
        private String _40px3X;
        @SerializedName("44px@3X")
        @Expose
        private String _44px3X;
        @SerializedName("88px@3X")
        @Expose
        private String _88px3X;
        @SerializedName("original")
        @Expose
        private String original;

        public String get30px() {
            return _30px;
        }

        public void set30px(String _30px) {
            this._30px = _30px;
        }

        public String get32px() {
            return _32px;
        }

        public void set32px(String _32px) {
            this._32px = _32px;
        }

        public String get40px() {
            return _40px;
        }

        public void set40px(String _40px) {
            this._40px = _40px;
        }

        public String get44px() {
            return _44px;
        }

        public void set44px(String _44px) {
            this._44px = _44px;
        }

        public String get48px() {
            return _48px;
        }

        public void set48px(String _48px) {
            this._48px = _48px;
        }

        public String get50px() {
            return _50px;
        }

        public void set50px(String _50px) {
            this._50px = _50px;
        }

        public String get60px() {
            return _60px;
        }

        public void set60px(String _60px) {
            this._60px = _60px;
        }

        public String get64px() {
            return _64px;
        }

        public void set64px(String _64px) {
            this._64px = _64px;
        }

        public String get73px() {
            return _73px;
        }

        public void set73px(String _73px) {
            this._73px = _73px;
        }

        public String get80px() {
            return _80px;
        }

        public void set80px(String _80px) {
            this._80px = _80px;
        }

        public String get88px() {
            return _88px;
        }

        public void set88px(String _88px) {
            this._88px = _88px;
        }

        public String get96px() {
            return _96px;
        }

        public void set96px(String _96px) {
            this._96px = _96px;
        }

        public String get100px() {
            return _100px;
        }

        public void set100px(String _100px) {
            this._100px = _100px;
        }

        public String get110px() {
            return _110px;
        }

        public void set110px(String _110px) {
            this._110px = _110px;
        }

        public String get120px() {
            return _120px;
        }

        public void set120px(String _120px) {
            this._120px = _120px;
        }

        public String get132px() {
            return _132px;
        }

        public void set132px(String _132px) {
            this._132px = _132px;
        }

        public String get146px() {
            return _146px;
        }

        public void set146px(String _146px) {
            this._146px = _146px;
        }

        public String get160px() {
            return _160px;
        }

        public void set160px(String _160px) {
            this._160px = _160px;
        }

        public String get176px() {
            return _176px;
        }

        public void set176px(String _176px) {
            this._176px = _176px;
        }

        public String get220px() {
            return _220px;
        }

        public void set220px(String _220px) {
            this._220px = _220px;
        }

        public String get264px() {
            return _264px;
        }

        public void set264px(String _264px) {
            this._264px = _264px;
        }

        public String get32px2X() {
            return _32px2X;
        }

        public void set32px2X(String _32px2X) {
            this._32px2X = _32px2X;
        }

        public String get40px2X() {
            return _40px2X;
        }

        public void set40px2X(String _40px2X) {
            this._40px2X = _40px2X;
        }

        public String get44px2X() {
            return _44px2X;
        }

        public void set44px2X(String _44px2X) {
            this._44px2X = _44px2X;
        }

        public String get88px2X() {
            return _88px2X;
        }

        public void set88px2X(String _88px2X) {
            this._88px2X = _88px2X;
        }

        public String get32px3X() {
            return _32px3X;
        }

        public void set32px3X(String _32px3X) {
            this._32px3X = _32px3X;
        }

        public String get40px3X() {
            return _40px3X;
        }

        public void set40px3X(String _40px3X) {
            this._40px3X = _40px3X;
        }

        public String get44px3X() {
            return _44px3X;
        }

        public void set44px3X(String _44px3X) {
            this._44px3X = _44px3X;
        }

        public String get88px3X() {
            return _88px3X;
        }

        public void set88px3X(String _88px3X) {
            this._88px3X = _88px3X;
        }

        public String getOriginal() {
            return original;
        }

        public void setOriginal(String original) {
            this.original = original;
        }

    }

    public class ImageUrl_ {

        @SerializedName("30px")
        @Expose
        private String _30px;
        @SerializedName("32px")
        @Expose
        private String _32px;
        @SerializedName("40px")
        @Expose
        private String _40px;
        @SerializedName("44px")
        @Expose
        private String _44px;
        @SerializedName("48px")
        @Expose
        private String _48px;
        @SerializedName("50px")
        @Expose
        private String _50px;
        @SerializedName("60px")
        @Expose
        private String _60px;
        @SerializedName("64px")
        @Expose
        private String _64px;
        @SerializedName("73px")
        @Expose
        private String _73px;
        @SerializedName("80px")
        @Expose
        private String _80px;
        @SerializedName("88px")
        @Expose
        private String _88px;
        @SerializedName("96px")
        @Expose
        private String _96px;
        @SerializedName("100px")
        @Expose
        private String _100px;
        @SerializedName("110px")
        @Expose
        private String _110px;
        @SerializedName("120px")
        @Expose
        private String _120px;
        @SerializedName("132px")
        @Expose
        private String _132px;
        @SerializedName("146px")
        @Expose
        private String _146px;
        @SerializedName("160px")
        @Expose
        private String _160px;
        @SerializedName("176px")
        @Expose
        private String _176px;
        @SerializedName("220px")
        @Expose
        private String _220px;
        @SerializedName("264px")
        @Expose
        private String _264px;
        @SerializedName("32px@2X")
        @Expose
        private String _32px2X;
        @SerializedName("40px@2X")
        @Expose
        private String _40px2X;
        @SerializedName("44px@2X")
        @Expose
        private String _44px2X;
        @SerializedName("88px@2X")
        @Expose
        private String _88px2X;
        @SerializedName("32px@3X")
        @Expose
        private String _32px3X;
        @SerializedName("40px@3X")
        @Expose
        private String _40px3X;
        @SerializedName("44px@3X")
        @Expose
        private String _44px3X;
        @SerializedName("88px@3X")
        @Expose
        private String _88px3X;
        @SerializedName("original")
        @Expose
        private String original;

        public String get30px() {
            return _30px;
        }

        public void set30px(String _30px) {
            this._30px = _30px;
        }

        public String get32px() {
            return _32px;
        }

        public void set32px(String _32px) {
            this._32px = _32px;
        }

        public String get40px() {
            return _40px;
        }

        public void set40px(String _40px) {
            this._40px = _40px;
        }

        public String get44px() {
            return _44px;
        }

        public void set44px(String _44px) {
            this._44px = _44px;
        }

        public String get48px() {
            return _48px;
        }

        public void set48px(String _48px) {
            this._48px = _48px;
        }

        public String get50px() {
            return _50px;
        }

        public void set50px(String _50px) {
            this._50px = _50px;
        }

        public String get60px() {
            return _60px;
        }

        public void set60px(String _60px) {
            this._60px = _60px;
        }

        public String get64px() {
            return _64px;
        }

        public void set64px(String _64px) {
            this._64px = _64px;
        }

        public String get73px() {
            return _73px;
        }

        public void set73px(String _73px) {
            this._73px = _73px;
        }

        public String get80px() {
            return _80px;
        }

        public void set80px(String _80px) {
            this._80px = _80px;
        }

        public String get88px() {
            return _88px;
        }

        public void set88px(String _88px) {
            this._88px = _88px;
        }

        public String get96px() {
            return _96px;
        }

        public void set96px(String _96px) {
            this._96px = _96px;
        }

        public String get100px() {
            return _100px;
        }

        public void set100px(String _100px) {
            this._100px = _100px;
        }

        public String get110px() {
            return _110px;
        }

        public void set110px(String _110px) {
            this._110px = _110px;
        }

        public String get120px() {
            return _120px;
        }

        public void set120px(String _120px) {
            this._120px = _120px;
        }

        public String get132px() {
            return _132px;
        }

        public void set132px(String _132px) {
            this._132px = _132px;
        }

        public String get146px() {
            return _146px;
        }

        public void set146px(String _146px) {
            this._146px = _146px;
        }

        public String get160px() {
            return _160px;
        }

        public void set160px(String _160px) {
            this._160px = _160px;
        }

        public String get176px() {
            return _176px;
        }

        public void set176px(String _176px) {
            this._176px = _176px;
        }

        public String get220px() {
            return _220px;
        }

        public void set220px(String _220px) {
            this._220px = _220px;
        }

        public String get264px() {
            return _264px;
        }

        public void set264px(String _264px) {
            this._264px = _264px;
        }

        public String get32px2X() {
            return _32px2X;
        }

        public void set32px2X(String _32px2X) {
            this._32px2X = _32px2X;
        }

        public String get40px2X() {
            return _40px2X;
        }

        public void set40px2X(String _40px2X) {
            this._40px2X = _40px2X;
        }

        public String get44px2X() {
            return _44px2X;
        }

        public void set44px2X(String _44px2X) {
            this._44px2X = _44px2X;
        }

        public String get88px2X() {
            return _88px2X;
        }

        public void set88px2X(String _88px2X) {
            this._88px2X = _88px2X;
        }

        public String get32px3X() {
            return _32px3X;
        }

        public void set32px3X(String _32px3X) {
            this._32px3X = _32px3X;
        }

        public String get40px3X() {
            return _40px3X;
        }

        public void set40px3X(String _40px3X) {
            this._40px3X = _40px3X;
        }

        public String get44px3X() {
            return _44px3X;
        }

        public void set44px3X(String _44px3X) {
            this._44px3X = _44px3X;
        }

        public String get88px3X() {
            return _88px3X;
        }

        public void set88px3X(String _88px3X) {
            this._88px3X = _88px3X;
        }

        public String getOriginal() {
            return original;
        }

        public void setOriginal(String original) {
            this.original = original;
        }

    }

    public class Maker {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("username")
        @Expose
        private String username;
        @SerializedName("headline")
        @Expose
        private Object headline;
        @SerializedName("twitter_username")
        @Expose
        private String twitterUsername;
        @SerializedName("website_url")
        @Expose
        private Object websiteUrl;
        @SerializedName("profile_url")
        @Expose
        private String profileUrl;
        @SerializedName("image_url")
        @Expose
        private ImageUrl imageUrl;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public Object getHeadline() {
            return headline;
        }

        public void setHeadline(Object headline) {
            this.headline = headline;
        }

        public String getTwitterUsername() {
            return twitterUsername;
        }

        public void setTwitterUsername(String twitterUsername) {
            this.twitterUsername = twitterUsername;
        }

        public Object getWebsiteUrl() {
            return websiteUrl;
        }

        public void setWebsiteUrl(Object websiteUrl) {
            this.websiteUrl = websiteUrl;
        }

        public String getProfileUrl() {
            return profileUrl;
        }

        public void setProfileUrl(String profileUrl) {
            this.profileUrl = profileUrl;
        }

        public ImageUrl getImageUrl() {
            return imageUrl;
        }

        public void setImageUrl(ImageUrl imageUrl) {
            this.imageUrl = imageUrl;
        }

    }

    public class Metadata {


    }

    public class Post implements Parcelable {

        @SerializedName("category_id")
        @Expose
        private Integer categoryId;
        @SerializedName("comments_count")
        @Expose
        private Integer commentsCount;
        @SerializedName("day")
        @Expose
        private String day;
        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("product_state")
        @Expose
        private String productState;
        @SerializedName("tagline")
        @Expose
        private String tagline;
        @SerializedName("ios_featured_at")
        @Expose
        private Object iosFeaturedAt;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("current_user")
        @Expose
        private CurrentUser currentUser;
        @SerializedName("discussion_url")
        @Expose
        private String discussionUrl;
        @SerializedName("exclusive")
        @Expose
        private Object exclusive;
        @SerializedName("featured")
        @Expose
        private Boolean featured;
        @SerializedName("maker_inside")
        @Expose
        private Boolean makerInside;
        @SerializedName("makers")
        @Expose
        private List<Maker> makers = null;
        @SerializedName("platforms")
        @Expose
        private List<Object> platforms = null;
        @SerializedName("topics")
        @Expose
        private List<Topic> topics = null;
        @SerializedName("redirect_url")
        @Expose
        private String redirectUrl;
        @SerializedName("screenshot_url")
        @Expose
        private ScreenshotUrl screenshotUrl;
        @SerializedName("thumbnail")
        @Expose
        private Thumbnail thumbnail;
        @SerializedName("user")
        @Expose
        private User user;
        @SerializedName("votes_count")
        @Expose
        private Integer votesCount;

        protected Post(Parcel in) {
            day = in.readString();
            name = in.readString();
            productState = in.readString();
            tagline = in.readString();
            createdAt = in.readString();
            discussionUrl = in.readString();
            redirectUrl = in.readString();
        }

        public final Creator<Post> CREATOR = new Creator<Post>() {
            @Override
            public Post createFromParcel(Parcel in) {
                return new Post(in);
            }

            @Override
            public Post[] newArray(int size) {
                return new Post[size];
            }
        };

        public Integer getCategoryId() {
            return categoryId;
        }

        public void setCategoryId(Integer categoryId) {
            this.categoryId = categoryId;
        }

        public Integer getCommentsCount() {
            return commentsCount;
        }

        public void setCommentsCount(Integer commentsCount) {
            this.commentsCount = commentsCount;
        }

        public String getDay() {
            return day;
        }

        public void setDay(String day) {
            this.day = day;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getProductState() {
            return productState;
        }

        public void setProductState(String productState) {
            this.productState = productState;
        }

        public String getTagline() {
            return tagline;
        }

        public void setTagline(String tagline) {
            this.tagline = tagline;
        }

        public Object getIosFeaturedAt() {
            return iosFeaturedAt;
        }

        public void setIosFeaturedAt(Object iosFeaturedAt) {
            this.iosFeaturedAt = iosFeaturedAt;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public CurrentUser getCurrentUser() {
            return currentUser;
        }

        public void setCurrentUser(CurrentUser currentUser) {
            this.currentUser = currentUser;
        }

        public String getDiscussionUrl() {
            return discussionUrl;
        }

        public void setDiscussionUrl(String discussionUrl) {
            this.discussionUrl = discussionUrl;
        }

        public Object getExclusive() {
            return exclusive;
        }

        public void setExclusive(Object exclusive) {
            this.exclusive = exclusive;
        }

        public Boolean getFeatured() {
            return featured;
        }

        public void setFeatured(Boolean featured) {
            this.featured = featured;
        }

        public Boolean getMakerInside() {
            return makerInside;
        }

        public void setMakerInside(Boolean makerInside) {
            this.makerInside = makerInside;
        }

        public List<Maker> getMakers() {
            return makers;
        }

        public void setMakers(List<Maker> makers) {
            this.makers = makers;
        }

        public List<Object> getPlatforms() {
            return platforms;
        }

        public void setPlatforms(List<Object> platforms) {
            this.platforms = platforms;
        }

        public List<Topic> getTopics() {
            return topics;
        }

        public void setTopics(List<Topic> topics) {
            this.topics = topics;
        }

        public String getRedirectUrl() {
            return redirectUrl;
        }

        public void setRedirectUrl(String redirectUrl) {
            this.redirectUrl = redirectUrl;
        }

        public ScreenshotUrl getScreenshotUrl() {
            return screenshotUrl;
        }

        public void setScreenshotUrl(ScreenshotUrl screenshotUrl) {
            this.screenshotUrl = screenshotUrl;
        }

        public Thumbnail getThumbnail() {
            return thumbnail;
        }

        public void setThumbnail(Thumbnail thumbnail) {
            this.thumbnail = thumbnail;
        }

        public User getUser() {
            return user;
        }

        public void setUser(User user) {
            this.user = user;
        }

        public Integer getVotesCount() {
            return votesCount;
        }

        public void setVotesCount(Integer votesCount) {
            this.votesCount = votesCount;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(day);
            dest.writeString(name);
            dest.writeString(productState);
            dest.writeString(tagline);
            dest.writeString(createdAt);
            dest.writeString(discussionUrl);
            dest.writeString(redirectUrl);
        }
    }
}


