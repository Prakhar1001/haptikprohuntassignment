package example.com.haptikproducthunt.network;

import example.com.haptikproducthunt.network.apimodels.CommentResponse;
import example.com.haptikproducthunt.network.apimodels.PostResponse;

public class ResultNotifiers {
    /**
     * Result notifier for API service.
     * Extend this notifier for different services.
     */
    public interface IBaseNotifier {
        void onErrorMessage(String errorMessage);

        void onSessionExpired(String errorMessage);
    }

    /**
     * post success result notifier.
     */
    public interface IPostResultNotifier extends IBaseNotifier {
        void onPostResponse(PostResponse.Response response);
    }

    /**
     * post success result notifier.
     */
    public interface ICommentResultNotifier extends IBaseNotifier {
        void onCommentReceiveResponse(CommentResponse response);
    }


}
