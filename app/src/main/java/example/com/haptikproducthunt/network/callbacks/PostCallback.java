package example.com.haptikproducthunt.network.callbacks;


import example.com.haptikproducthunt.network.BaseCallback;
import example.com.haptikproducthunt.network.ResultNotifiers;
import example.com.haptikproducthunt.network.apimodels.PostResponse;
import retrofit2.Call;
import retrofit2.Response;

/**
 * class extends for handle Login response.
 */

public class PostCallback extends BaseCallback<PostResponse.Response> {

    public PostCallback(ResultNotifiers.IBaseNotifier notifier) {
        super(notifier);
    }

    /**
     * login success response callback.
     * @param model    response model
     * @param call     call
     * @param response response
     */

    @Override
    public void onSuccessResponse(PostResponse.Response model, Call<PostResponse.Response> call, Response<PostResponse.Response> response) {
        if (model != null) {
            notifyResponse(model);
        } else {
            notifyError("Failed to fetch response");
        }
    }



    /**
     * notify success response.
     * @param model
     */
    private void notifyResponse(PostResponse.Response model) {
        if (mNotifier != null && mNotifier instanceof ResultNotifiers.IPostResultNotifier) {
            ((ResultNotifiers.IPostResultNotifier) mNotifier).onPostResponse(model);
        }
    }
}
