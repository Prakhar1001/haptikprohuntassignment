package example.com.haptikproducthunt.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;

import example.com.haptikproducthunt.network.apimodels.CommentResponse;
import example.com.haptikproducthunt.network.apimodels.PostResponse;


/**
 * Created by Prakhar on 01-Jul-17.
 */
public class PreferenceUtils {

    public static final String LOADED_FRAGMENT_KEY = "LoadedFragment";
    public static final String SAVE_POST_LIST_KEY = "postlist";
    public static final String SAVE_DATE_POST_LIST_KEY = "datepostlist";


    public static final int HOME_FRAGMENT = 1;
    public static final int COMMENT_FRAGMENT = 2;

    protected static final String TAG = PreferenceUtils.class.getName();
    private static final String PREFERENCE_NAME = "HaptikAssignment";


    public static String getString(Context context, String key) {
        String value = null;
        SharedPreferences sharedpreferences = context.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE);
        if (sharedpreferences.contains(key)) {
            value = sharedpreferences.getString(key, null);
        }
        return value;
    }

    public static void saveString(Context context, String key, String value) {
        SharedPreferences sharedpreferences = context.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor prefEditor = sharedpreferences.edit();
        prefEditor.putString(key, value);
        prefEditor.apply();
    }


    public static void savePostList(Context context, String key,ArrayList<PostResponse.Post> productList) {
        SharedPreferences sharedpreferences = context.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor prefEditor = sharedpreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(productList);
        prefEditor.putString(key, json);
        prefEditor.apply();
    }

    public static ArrayList<PostResponse.Post> getPostList(Context context, String key) {
        String json = null;
        SharedPreferences sharedpreferences = context.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE);
        if (sharedpreferences.contains(key)) {
            json = sharedpreferences.getString(key, null);
        }
        Type type = new TypeToken<ArrayList<PostResponse.Post>>() {
        }.getType();
        Gson gson = new Gson();
        ArrayList<PostResponse.Post> arrayList = gson.fromJson(json, type);
        return arrayList;
    }


    public static void saveCurrentlyLoadedFragment(Context context, String key, int value) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor prefEditor = sharedPreferences.edit();
        prefEditor.putInt(key, value);
        prefEditor.apply();
    }

    public static int getCurrentLoadedFragment(Context context, String key) {
        int value = 0;
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE);
        if (sharedPreferences.contains(key)) {
            value = sharedPreferences.getInt(key, -1);
        }
        return value;
    }
}
