package example.com.haptikproducthunt.utils;

/**
 * Created by Prakhar on 01-Jul-17.
 */

public class Constants {


    public static final int SUCCESS_RESULT = 0;

    public static final int FAILURE_RESULT = 1;

    public static final int SHOW_HELP_WINDOW = 2;


    public static String getToken() {
        return token;
    }

    private static String token = "Bearer 83763fb4dd21e4c42d7254909886e1db1c48c99f1b9e129543ac03617005abc1";



    public interface ResponseCodes {
        int SUCCESS_CODE = 200;
        String STATUS_SUCCESS = "Success";
        String STATUS_FAILURE = "Failure";
    }


}
