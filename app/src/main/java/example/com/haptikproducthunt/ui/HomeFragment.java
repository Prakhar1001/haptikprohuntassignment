package example.com.haptikproducthunt.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputFilter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import java.text.DateFormat;
import java.text.ParseException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import example.com.haptikproducthunt.R;
import example.com.haptikproducthunt.adapter.ProductListAdapter;
import example.com.haptikproducthunt.base.BaseFragment;
import example.com.haptikproducthunt.base.IToolbar;
import example.com.haptikproducthunt.network.HaptikServiceManager;
import example.com.haptikproducthunt.network.ResultNotifiers;
import example.com.haptikproducthunt.network.apimodels.PostResponse;
import example.com.haptikproducthunt.utils.DeviceUtils;
import example.com.haptikproducthunt.utils.InputFilterMinMax;
import example.com.haptikproducthunt.utils.NetworkUtils;
import example.com.haptikproducthunt.utils.PreferenceUtils;


/**
 * Created by Prakhar on 30-Jun-17.
 */


public class HomeFragment extends BaseFragment implements IToolbar, CompoundButton.OnCheckedChangeListener,
        View.OnClickListener, ProductListAdapter.OnItemClickListner {

    private RelativeLayout calenderLayout;
    private RecyclerView mProductList;
    private TextView mDate;
    private CheckBox mDaysAgoCheckBox;
    private CheckBox mSpecificCheckBox;
    private TextView mSubmitFilter;
    private EditText mDaysAgoEdittext, mSpecificDayEdittext, mSpecificMonthEdittext, mSpecificYearEdittext;
    private LinearLayout mSpecificDayLayout;
    private boolean daysOffFlag, specificDayFlag;
    private PostResponse.Response response = null;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.home_fragment, container, false);
        return view;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        super.onViewCreated(view, savedInstanceState);
        mToolbarInteractionListener.updateToolbar(this);
        PreferenceUtils.saveCurrentlyLoadedFragment(getActivity().getApplicationContext(), PreferenceUtils.LOADED_FRAGMENT_KEY,
                PreferenceUtils.HOME_FRAGMENT);


        mSpecificDayLayout = (LinearLayout) view.findViewById(R.id.specific_day_layout);
        mDaysAgoEdittext = (EditText) view.findViewById(R.id.days_ago_edittext);
        mSpecificDayEdittext = (EditText) view.findViewById(R.id.sp_day_edittext);
        mSpecificMonthEdittext = (EditText) view.findViewById(R.id.sp_month_edittext);
        mSpecificYearEdittext = (EditText) view.findViewById(R.id.sp_year_edittext);
        mDaysAgoCheckBox = (CheckBox) view.findViewById(R.id.days_ago_checkbox);
        mSpecificCheckBox = (CheckBox) view.findViewById(R.id.specific_day_checkbox);
        mSubmitFilter = (TextView) view.findViewById(R.id.submit_filter);
        calenderLayout = (RelativeLayout) view.findViewById(R.id.calender_layout);
        mDate = (TextView) view.findViewById(R.id.date_browse);
        mProductList = (RecyclerView) view.findViewById(R.id.products_list);

        mSpecificDayEdittext.setFilters(new InputFilter[]{new InputFilterMinMax("1", "31")});
        mSpecificMonthEdittext.setFilters(new InputFilter[]{new InputFilterMinMax("1", "12")});
        mSpecificYearEdittext.setFilters(new InputFilter[]{new InputFilterMinMax("1", "2017")});

        mDaysAgoCheckBox.setOnCheckedChangeListener(this);
        mSpecificCheckBox.setOnCheckedChangeListener(this);
        mSubmitFilter.setOnClickListener(this);

        if (savedInstanceState == null) {
            if (response == null)
                callPostsForTodayAPIRequest();
            else {
                populateResponseData();
            }
        } else {
            setProductData(PreferenceUtils.getPostList(getActivity(), PreferenceUtils.SAVE_POST_LIST_KEY));
            if (PreferenceUtils.getString(getActivity(), PreferenceUtils.SAVE_DATE_POST_LIST_KEY) != null)
                mDate.setText(PreferenceUtils.getString(getActivity(), PreferenceUtils.SAVE_DATE_POST_LIST_KEY));
        }

    }

    private void populateResponseData() {
        setProductData(response.getPosts());
        if (response.getPosts().size() > 0)
            setDateBrowsed(response.getPosts().get(0).getCreatedAt());
        else
            Toast.makeText(getActivity(), "No Posts Found for this Date", Toast.LENGTH_LONG).show();
    }


    private void callPostsForTodayAPIRequest() {
        mFragmentInteractionListener.showBlockingProgressBar();
        if (!NetworkUtils.isNetworkConnected(getActivity())) {
            Toast.makeText(getActivity(), "No Internet Connection", Toast.LENGTH_LONG).show();
            if (PreferenceUtils.getPostList(getActivity(), PreferenceUtils.SAVE_POST_LIST_KEY) != null) {
                setProductData(PreferenceUtils.getPostList(getActivity(), PreferenceUtils.SAVE_POST_LIST_KEY));
                if (PreferenceUtils.getString(getActivity(), PreferenceUtils.SAVE_DATE_POST_LIST_KEY) != null)
                    mDate.setText(PreferenceUtils.getString(getActivity(), PreferenceUtils.SAVE_DATE_POST_LIST_KEY));
                Toast.makeText(getActivity(), "Loading locally stored data", Toast.LENGTH_LONG).show();
            }
            mFragmentInteractionListener.hideBlockingProgressBar();

        } else {
            HaptikServiceManager.getInstance().performgetPostsForTodayOperation(getPostAPIOperationResponseListener());
        }
    }


    private void callPostsForDaysAgoAPIRequest(String daysOff) {
        mFragmentInteractionListener.showBlockingProgressBar();
        if (!NetworkUtils.isNetworkConnected(getActivity())) {
            Toast.makeText(getActivity(), "No Internet Connection, Retry", Toast.LENGTH_LONG).show();
            mFragmentInteractionListener.hideBlockingProgressBar();
        } else {
            HaptikServiceManager.getInstance().performgetPostsForDaysAgoOperation(daysOff, getPostAPIOperationResponseListener());
        }
    }


    private void callPostsForSpecificDayAPIRequest(String date) {
        mFragmentInteractionListener.showBlockingProgressBar();
        if (!NetworkUtils.isNetworkConnected(getActivity())) {
            Toast.makeText(getActivity(), "No Internet Connection, Retry", Toast.LENGTH_LONG).show();
            mFragmentInteractionListener.hideBlockingProgressBar();
        } else {
            HaptikServiceManager.getInstance().performgetPostsForSpecificDayOperation(date, getPostAPIOperationResponseListener());

        }
    }


    ResultNotifiers.IPostResultNotifier getPostAPIOperationResponseListener() {
        return new ResultNotifiers.IPostResultNotifier() {
            @Override
            public void onPostResponse(PostResponse.Response response) {
                mFragmentInteractionListener.hideBlockingProgressBar();
                setResponse(response);
                saveListToPreference(response.getPosts());
                DeviceUtils.hideKeyboard(getActivity());
                populateResponseData();
            }

            @Override
            public void onErrorMessage(String errorMessage) {
                mFragmentInteractionListener.hideBlockingProgressBar();
                DeviceUtils.hideKeyboard(getActivity());
                Toast.makeText(getActivity(), "error", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onSessionExpired(String errorMessage) {
                mFragmentInteractionListener.hideBlockingProgressBar();
                DeviceUtils.hideKeyboard(getActivity());
                Toast.makeText(getActivity(), "session expired", Toast.LENGTH_LONG).show();
            }
        };
    }

    private void saveListToPreference(List<PostResponse.Post> posts) {
        PreferenceUtils.savePostList(getActivity().getApplicationContext(), PreferenceUtils.SAVE_POST_LIST_KEY,
                (ArrayList<PostResponse.Post>) posts);
    }

    private void setResponse(PostResponse.Response response) {
        this.response = response;
    }

    private void setDateBrowsed(String createdAt) {
        try {
            DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.US);
            Date date = inputFormat.parse(createdAt);
            DateFormat outputFormat = new SimpleDateFormat("EEE, MMM d, ''yy");
            String outputString = outputFormat.format(date);

            PreferenceUtils.saveString(getActivity(), PreferenceUtils.SAVE_DATE_POST_LIST_KEY, outputString);

            mDate.setText(outputString);
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }

    }

    private void setProductData(List<PostResponse.Post> posts) {
        if (posts != null) {
            ProductListAdapter mAdapter = new ProductListAdapter(getActivity(), posts);

            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
            mProductList.setLayoutManager(mLayoutManager);
            mProductList.setItemAnimator(new DefaultItemAnimator());
            mProductList.setAdapter(mAdapter);
            mAdapter.setOnCardClickListner(this);
        }
    }


    @Override
    public int getHeaderLeftIconId() {
        return 0;
    }

    @Override
    public int getHeaderRightIconId() {
        return R.drawable.ic_insert_invitation_white_24dp;
    }

    @Override
    public View.OnClickListener getHeaderViewClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.header_right_img:
                        if (!NetworkUtils.isNetworkConnected(getActivity())) {
                            Toast.makeText(getActivity(), "Filters need Internet Connection", Toast.LENGTH_LONG).show();
                        } else {
                            if (calenderLayout.isShown())
                                calenderLayout.setVisibility(View.GONE);
                            else
                                calenderLayout.setVisibility(View.VISIBLE);
                        }
                        break;
                    default:
                        Toast.makeText(getActivity(), "No such option found", Toast.LENGTH_LONG).show();
                        break;
                }

            }
        };
    }

    @Override
    public String getToolbarTitleString() {
        return "Products";
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()) {
            case R.id.days_ago_checkbox:
                if (isChecked) {
                    mDaysAgoEdittext.setVisibility(View.VISIBLE);
                    mSpecificCheckBox.setEnabled(false);
                    mSubmitFilter.setVisibility(View.VISIBLE);
                    daysOffFlag = true;
                } else {
                    mSpecificCheckBox.setEnabled(true);
                    mDaysAgoEdittext.setVisibility(View.GONE);
                    mSubmitFilter.setVisibility(View.GONE);
                    daysOffFlag = false;
                }
                break;
            case R.id.specific_day_checkbox:
                if (isChecked) {
                    specificDayFlag = true;
                    mDaysAgoCheckBox.setEnabled(false);
                    mSpecificDayLayout.setVisibility(View.VISIBLE);
                    mSubmitFilter.setVisibility(View.VISIBLE);
                } else {
                    specificDayFlag = false;
                    mDaysAgoCheckBox.setEnabled(true);
                    mSpecificDayLayout.setVisibility(View.GONE);
                    mSubmitFilter.setVisibility(View.GONE);
                }
                break;
            default:
                Toast.makeText(getActivity(), "No such option found", Toast.LENGTH_LONG).show();
                break;
        }

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.submit_filter:
                if (daysOffFlag && !mDaysAgoEdittext.getText().equals("")) {
                    mDate.setText(mDaysAgoEdittext.getText().toString() + " Days Ago");
                    callPostsForDaysAgoAPIRequest(mDaysAgoEdittext.getText().toString());
                } else {
                    calenderLayout.setVisibility(View.GONE);
                }

                if (specificDayFlag && !mSpecificDayEdittext.getText().equals("") &&
                        !mSpecificMonthEdittext.getText().equals("") &&
                        !mSpecificYearEdittext.getText().equals("")) {
                    String date = mSpecificYearEdittext.getText().toString() + "-" + mSpecificMonthEdittext.getText().toString() + "-" +
                            mSpecificDayEdittext.getText().toString();
                    mDate.setText(date);
                    callPostsForSpecificDayAPIRequest(date);
                } else {
                    calenderLayout.setVisibility(View.GONE);
                }

                break;
            default:
                Toast.makeText(getActivity(), "No such option found", Toast.LENGTH_LONG).show();
                break;
        }

    }


    @Override
    public void OnItemClicked(View view, int position) {
        mFragmentInteractionListener.showBlockingProgressBar();
        mFragmentInteractionListener.loadFragment(R.id.fragment_container,
                CommentFragment.getInstance(PreferenceUtils.getPostList(getActivity(), PreferenceUtils.SAVE_POST_LIST_KEY)
                        .get(position).getId().toString()),
                CommentFragment.class.getSimpleName(),
                R.anim.enter_anim, R.anim.exit_anim, FragmentTransactionType.ADD_TO_BACK_STACK_AND_REPLACE);
    }
}
