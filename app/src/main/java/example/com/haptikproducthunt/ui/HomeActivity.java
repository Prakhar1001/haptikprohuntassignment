package example.com.haptikproducthunt.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;

import example.com.haptikproducthunt.R;
import example.com.haptikproducthunt.base.BaseActivity;
import example.com.haptikproducthunt.base.BaseFragment;
import example.com.haptikproducthunt.utils.PreferenceUtils;

/**
 * Created by Prakhar on 30-Jun-17.
 */

public class HomeActivity extends BaseActivity {

    Fragment fragment;

    @Override
    protected int getHeaderLayoutId() {
        return getCommonHeaderLayoutId();
    }

    @Override
    protected void initHeaderView(View view) {
        initCommonHeaderViews(view);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addContentView(R.layout.home_activity);


        if (savedInstanceState != null) {
            fragment = getSupportFragmentManager().getFragment(savedInstanceState, "KEY");
            /*if (fragment instanceof TestResultFragment) {
                loadFragment(R.id.fragment_container, (BaseFragment) fragment, TestResultFragment.class.getSimpleName(),
                        0, 0, BaseFragment.FragmentTransactionType.REPLACE);
            }
            */

        } else {

            int fragmentNumber = PreferenceUtils.getCurrentLoadedFragment(this, PreferenceUtils.LOADED_FRAGMENT_KEY);


            loadFragment(R.id.fragment_container, new HomeFragment(), HomeFragment.class.getSimpleName(),
                    0, 0, BaseFragment.FragmentTransactionType.ADD);

        }
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        /*fragment = getSupportFragmentManager().findFragmentByTag(ToddTestFragment.class.getSimpleName());
        if (fragment != null) {
            getSupportFragmentManager().putFragment(outState, "KEY", fragment);
        } else if (getSupportFragmentManager().findFragmentByTag(TestResultFragment.class.getSimpleName()) != null) {
            getSupportFragmentManager().putFragment(outState, "KEY", fragment);
        } */
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        PreferenceUtils.saveCurrentlyLoadedFragment(this.getApplicationContext(), PreferenceUtils.LOADED_FRAGMENT_KEY,
                PreferenceUtils.HOME_FRAGMENT);

    }
}

