package example.com.haptikproducthunt.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import example.com.haptikproducthunt.R;
import example.com.haptikproducthunt.adapter.CommentAdapter;
import example.com.haptikproducthunt.adapter.ProductListAdapter;
import example.com.haptikproducthunt.base.BaseFragment;
import example.com.haptikproducthunt.base.IToolbar;
import example.com.haptikproducthunt.network.HaptikServiceManager;
import example.com.haptikproducthunt.network.ResultNotifiers;
import example.com.haptikproducthunt.network.apimodels.CommentResponse;
import example.com.haptikproducthunt.network.apimodels.PostResponse;
import example.com.haptikproducthunt.utils.DeviceUtils;
import example.com.haptikproducthunt.utils.NetworkUtils;
import example.com.haptikproducthunt.utils.PreferenceUtils;

/**
 * Created by Prakhar on 01-Jul-17.
 */

public class CommentFragment extends BaseFragment implements IToolbar, View.OnClickListener {

    private ListView mlistView;
    private static final String POST_RESPONSE_KEY = "post_response";
    private String post_id;
    private CommentAdapter commentAdapter;
    private TextView mLoadMore;
    private int page_number;
    private ArrayList<CommentResponse.Comment> commentsList;


    public static CommentFragment getInstance(String post_id) {
        Bundle args = new Bundle();
        args.putString(POST_RESPONSE_KEY, post_id);
        CommentFragment fragment = new CommentFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            post_id = getArguments().getString(POST_RESPONSE_KEY);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.comment_fragment, container, false);
        return view;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        super.onViewCreated(view, savedInstanceState);
        mToolbarInteractionListener.updateToolbar(this);
        PreferenceUtils.saveCurrentlyLoadedFragment(getActivity().getApplicationContext(), PreferenceUtils.LOADED_FRAGMENT_KEY,
                PreferenceUtils.COMMENT_FRAGMENT);

        commentsList = new ArrayList<>();
        page_number = 1;

        Map<String, String> data = new HashMap<>();
        data.put("search[post_id]", post_id);
        data.put("page", String.valueOf(page_number));
        data.put("per_page", "5");

        mLoadMore = (TextView) view.findViewById(R.id.load_more);
        mLoadMore.setOnClickListener(this);

        mlistView = (ListView) view.findViewById(R.id.comment_list);
        callCommentsForSpecificPostsAPIRequest(data);

    }

    private void callCommentsForSpecificPostsAPIRequest(Map<String, String> parameters) {
        mFragmentInteractionListener.showBlockingProgressBar();
        if (!NetworkUtils.isNetworkConnected(getActivity())) {
            Toast.makeText(getActivity(), "No Internet Connection", Toast.LENGTH_LONG).show();
            mFragmentInteractionListener.hideBlockingProgressBar();
        } else {
            HaptikServiceManager.getInstance().performgetCommentsForPostOperation(parameters, getCommentsAPIOperationResponseListener());
        }
    }


    ResultNotifiers.ICommentResultNotifier getCommentsAPIOperationResponseListener() {
        return new ResultNotifiers.ICommentResultNotifier() {
            @Override
            public void onCommentReceiveResponse(CommentResponse response) {
                setCommentArrayListData(response);
            }

            @Override
            public void onErrorMessage(String errorMessage) {
                mFragmentInteractionListener.hideBlockingProgressBar();
                DeviceUtils.hideKeyboard(getActivity());
                Toast.makeText(getActivity(), "error", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onSessionExpired(String errorMessage) {
                mFragmentInteractionListener.hideBlockingProgressBar();
                DeviceUtils.hideKeyboard(getActivity());
                Toast.makeText(getActivity(), "session expired", Toast.LENGTH_LONG).show();
            }
        };
    }


    private void setCommentArrayListData(final CommentResponse response) {
        if (response.getComments().size() > 0) {
            for (CommentResponse.Comment comment : response.getComments()) {
                commentsList.add(comment);
            }
            //send arraylist populate with response
            commentAdapter = new CommentAdapter(getActivity(), commentsList);
        } else {
            Toast.makeText(getActivity(), "No more comments", Toast.LENGTH_LONG).show();
            mLoadMore.setVisibility(View.GONE);
        }


        mlistView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent whatsappIntent = new Intent(Intent.ACTION_SEND);
                whatsappIntent.setType("text/plain");
                whatsappIntent.setPackage("com.whatsapp");
                whatsappIntent.putExtra(Intent.EXTRA_TEXT, response.getComments().get(position).getBody());
                try {
                    getActivity().startActivity(whatsappIntent);
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(getActivity(), "Whatsapp have not been installed.", Toast.LENGTH_LONG).show();
                }
            }
        });

        mFragmentInteractionListener.hideBlockingProgressBar();
        mlistView.setAdapter(commentAdapter);
    }


    @Override
    public int getHeaderLeftIconId() {
        return R.drawable.lk_ic_arrow_back_white;
    }

    @Override
    public int getHeaderRightIconId() {
        return 0;
    }

    @Override
    public View.OnClickListener getHeaderViewClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.header_left_img:
                        getActivity().onBackPressed();
                        break;
                    default:
                        Toast.makeText(getActivity(), "No such option found", Toast.LENGTH_LONG).show();
                        break;
                }
            }
        };
    }

    @Override
    public String getToolbarTitleString() {
        return "Comments";
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.load_more:
                page_number++;

                Map<String, String> data = new HashMap<>();
                data.put("search[post_id]", post_id);
                data.put("page", String.valueOf(page_number));
                data.put("per_page", "5");

                callCommentsForSpecificPostsAPIRequest(data);

                break;
            default:
                Toast.makeText(getActivity(), "No such option found", Toast.LENGTH_LONG).show();
                break;
        }
    }
}
