package example.com.haptikproducthunt.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import example.com.haptikproducthunt.R;
import example.com.haptikproducthunt.network.apimodels.PostResponse;

/**
 * Created by Prakhar on 30-Jun-17.
 */

public class ProductListAdapter extends RecyclerView.Adapter<ProductListAdapter.MyViewHolder> {

    OnItemClickListner onItemClickListner;
    List<PostResponse.Post> postList;
    Context context;

    public ProductListAdapter(Context context, List<PostResponse.Post> postList) {
        this.context = context;
        this.postList = postList;
    }




    public class MyViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final ImageView mProductImageView;
        public final TextView mProductTitle;
        public final TextView mProductTagline;
        public final TextView mCommentCount;
        public final RelativeLayout mItemLayout;


        public MyViewHolder(View view) {
            super(view);
            mView = view;
            mItemLayout = (RelativeLayout) mView.findViewById(R.id.post_row_layout);
            mProductImageView = (ImageView) mView.findViewById(R.id.product_user_image);
            mProductTitle = (TextView) mView.findViewById(R.id.product_title);
            mProductTagline = (TextView) mView.findViewById(R.id.product_tagline);
            mCommentCount = (TextView) mView.findViewById(R.id.comment_count);
        }
    }


    @Override
    public ProductListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.home_product_row, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ProductListAdapter.MyViewHolder holder, final int position) {
        //set value to the viewholder components here

            holder.mProductTitle.setText(postList.get(position).getName());
            holder.mProductTagline.setText(postList.get(position).getTagline());
            holder.mCommentCount.setText(postList.get(position).getCommentsCount().toString());
            holder.mItemLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClickListner.OnItemClicked(v, position);
                }
            });

            Glide.with(context)
                    .load(postList.get(position).getThumbnail().getImageUrl())
                    .placeholder(R.mipmap.ic_launcher) // can also be a drawable
                    .error(R.mipmap.ic_launcher_round)
                    .into(holder.mProductImageView);

    }

    @Override
    public int getItemCount() {
        if (postList != null)
            return postList.size();
        else
            return 0;
    }

    public interface OnItemClickListner {
        void OnItemClicked(View view, int position);
    }

    public void setOnCardClickListner(OnItemClickListner onItemClickListner) {
        this.onItemClickListner = onItemClickListner;
    }

}

