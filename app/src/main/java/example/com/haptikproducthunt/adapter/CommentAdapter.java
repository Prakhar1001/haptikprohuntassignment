package example.com.haptikproducthunt.adapter;

import android.content.Context;
import android.content.Intent;
import android.database.DataSetObserver;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import example.com.haptikproducthunt.R;
import example.com.haptikproducthunt.network.apimodels.CommentResponse;

/**
 * Created by Prakhar on 01-Jul-17.
 */

public class CommentAdapter extends BaseAdapter {

    ArrayList<CommentResponse.Comment> commentArrayList;
    Context context;

    public CommentAdapter(Context context, ArrayList<CommentResponse.Comment> commentArrayList) {
        this.context = context;
        this.commentArrayList = commentArrayList;

    }


    @Override
    public int getCount() {
        return commentArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return commentArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(context).inflate(R.layout.comment_list_item, null);
        TextView comment_title = (TextView) convertView.findViewById(R.id.comment_body);
        comment_title.setText((position + 1) + ". " + commentArrayList.get(position).getBody());
        return convertView;

    }


}
