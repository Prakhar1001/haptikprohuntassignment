package example.com.haptikproducthunt.base;

import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;


/**
 * Created by Prakhar on 01-Jul-17.
 */

public interface IFragmentInteractionListener {

    void loadFragment(int fragmentContainerId, BaseFragment fragment,
                      @Nullable String tag, int enterAnimId, int exitAnimId,
                      BaseFragment.FragmentTransactionType fragmentTransactionType);

    void loadDialogFragment(DialogFragment fragment,
                            @Nullable String tag);

    void dismiss();

    void showBlockingProgressBar();

    void hideBlockingProgressBar();

    void hideKeyboard();

    void isToolBarRequired(boolean isRequired);

}
