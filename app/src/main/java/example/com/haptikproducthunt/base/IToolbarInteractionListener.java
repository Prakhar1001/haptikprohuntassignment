package example.com.haptikproducthunt.base;

/**
 * Created by Prakhar on 01-Jul-17.
 */

public interface IToolbarInteractionListener {
    void updateToolbar(IToolbar toolbar);
}
