package example.com.haptikproducthunt.base;

import android.view.View;


/**
 * Created by Prakhar on 01-Jul-17.
 */

public interface IToolbar {

    int getHeaderLeftIconId();

    int getHeaderRightIconId();

    View.OnClickListener getHeaderViewClickListener();

    String getToolbarTitleString();
}
